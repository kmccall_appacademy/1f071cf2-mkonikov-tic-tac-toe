class Board
  attr_accessor :grid

  def initialize(grid=Array.new(3) { Array.new(3) })
    @grid = grid
  end

  def place_mark(position, mark)
    if empty?(position)
      grid[position[0]][position[1]] = mark
    else
      #puts "Invalid move. That place is already taken."
    end
  end

  def empty?(position)
    grid[position[0]][position[1]].nil?
  end

  def row_winner
    grid[0..2].each do |row|
      return :X if row.all? { |cell| cell == :X }
      return :O if row.all? { |cell| cell == :O }
    end
    nil
  end

  def col_winner
    grid[0..2].transpose.each do |col|
      return :X if col.all? { |cell| cell == :X }
      return :O if col.all? { |cell| cell == :O }
    end
    nil
  end

  def diag_winner
    @diag1 = [grid[0][0], grid[1][1], grid[2][2]]
    @diag2 = [grid[0][2], grid[1][1], grid[2][0]]
    [@diag1, @diag2].each do |el|
      return :X if el.all? { |cell| cell == :X }
      return :O if el.all? { |cell| cell == :O }
    end
    nil
  end

  def winner
    return row_winner if !row_winner.nil?
    return col_winner if !col_winner.nil?
    diag_winner
  end

  def over?
    return true if !winner.nil?
    return true if @grid.flatten.none? { |el| el.nil? }
    false
  end

end
