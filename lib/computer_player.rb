class ComputerPlayer

  attr_accessor :name, :board, :mark

  def initialize(name, mark=:O)
    @name = name
    @mark = :O
  end

  def display(board)
    @board = board
    puts @board.grid
  end

  def get_move
    move = []

    (0..2).each do |row|
      (0..2).each do |cell|
        previous = @board.grid[row][cell].dup
        @board.place_mark([row, cell], @mark)
        if @board.winner == @mark
          @board.grid[row][cell] = previous
          return [row, cell]
        elsif previous.nil? && move.empty?
          move = [row, cell]
        end
        @board.grid[row][cell] = previous
      end
    end
    move

  end

end
