require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :current_player
  attr_reader :player_one, :player_two

  def initialize(player_one, player_two)
    @player_one = player_one
    @player_two = player_two
    @board = Board.new
    @current_player = player_one
  end

  def switch_players!
    self.current_player = current_player == player_one ? player_two : player_one
  end

  def play_turn
    @current_player.display(@board)
    @move = @current_player.get_move
    # CAN ADD THESE FOUR LINES TO ALLOW PLAYER TO REDO BAD MOVE. FAILS RSPES SO COMMENTING OUT
    #if !@board.empty?(@move)
    #  puts "That is not an empty spot. Please try a different move."
    #  @move = @current_player.get_move
    #end
    @mark = @current_player.mark
    @board.place_mark(@move, @mark)
    puts "#{@mark} has moved to #{@move}"
    self.switch_players!
  end

  def play
    current_player.display(@board)
    play_turn until @board.over?
    puts "Yay! #{@mark} wins the game! Addaboy!"
  end

end

#mendel = HumanPlayer.new("Mendel")
#comp = ComputerPlayer.new("Comp", :O)
#start = Game.new(mendel, comp)

#start.play
