class HumanPlayer
  attr_accessor :name, :mark

  def initialize(name, mark=:X)
    @name = name
    @mark = mark
  end

  def display(board)
    @board = board
    puts board.grid
  end

  def get_move
    puts "Hey #{mark}! Where would you like to place your piece? (Be sure to place in a format like this [0, 2])"
    move = gets.chomp
    move = move.split(", ").map(&:to_i)
  end

end
